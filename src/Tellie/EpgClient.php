<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Tellie;

/**
 */
class EpgClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Google\Protobuf\GPBEmpty $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function listDatasources(\Google\Protobuf\GPBEmpty $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.Epg/listDatasources',
        $argument,
        ['\Tellie\Epg\Api\ListDatasourcesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Tellie\Epg\Api\ListChannelsByDatasourceRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function listChannelsByDatasource(\Tellie\Epg\Api\ListChannelsByDatasourceRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.Epg/listChannelsByDatasource',
        $argument,
        ['\Tellie\Epg\Api\ListChannelsByDatasourceResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Tellie\Epg\Api\ListBroadcastsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function listBroadcasts(\Tellie\Epg\Api\ListBroadcastsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.Epg/listBroadcasts',
        $argument,
        ['\Tellie\Epg\Api\ListBroadcastsResponse', 'decode'],
        $metadata, $options);
    }

}
