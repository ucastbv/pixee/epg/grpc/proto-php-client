<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: epg/model/channel.proto

namespace Tellie\Epg\Model;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>tellie.epg.model.Channel</code>
 */
class Channel extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string id = 1;</code>
     */
    protected $id = '';
    /**
     * Generated from protobuf field <code>string name = 2;</code>
     */
    protected $name = '';
    /**
     * Generated from protobuf field <code>optional string logo = 3;</code>
     */
    protected $logo = null;
    /**
     * Generated from protobuf field <code>string defaultLocale = 4;</code>
     */
    protected $defaultLocale = '';
    /**
     * Generated from protobuf field <code>string stream_uri = 5;</code>
     */
    protected $stream_uri = '';
    /**
     * Generated from protobuf field <code>int32 default_pre_padding = 6;</code>
     */
    protected $default_pre_padding = 0;
    /**
     * Generated from protobuf field <code>int32 default_post_padding = 7;</code>
     */
    protected $default_post_padding = 0;
    /**
     * Generated from protobuf field <code>bool recordable = 8;</code>
     */
    protected $recordable = false;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $id
     *     @type string $name
     *     @type string $logo
     *     @type string $defaultLocale
     *     @type string $stream_uri
     *     @type int $default_pre_padding
     *     @type int $default_post_padding
     *     @type bool $recordable
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Epg\Model\Channel::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkString($var, True);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string name = 2;</code>
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Generated from protobuf field <code>string name = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setName($var)
    {
        GPBUtil::checkString($var, True);
        $this->name = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>optional string logo = 3;</code>
     * @return string
     */
    public function getLogo()
    {
        return isset($this->logo) ? $this->logo : '';
    }

    public function hasLogo()
    {
        return isset($this->logo);
    }

    public function clearLogo()
    {
        unset($this->logo);
    }

    /**
     * Generated from protobuf field <code>optional string logo = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setLogo($var)
    {
        GPBUtil::checkString($var, True);
        $this->logo = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string defaultLocale = 4;</code>
     * @return string
     */
    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    /**
     * Generated from protobuf field <code>string defaultLocale = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setDefaultLocale($var)
    {
        GPBUtil::checkString($var, True);
        $this->defaultLocale = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string stream_uri = 5;</code>
     * @return string
     */
    public function getStreamUri()
    {
        return $this->stream_uri;
    }

    /**
     * Generated from protobuf field <code>string stream_uri = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setStreamUri($var)
    {
        GPBUtil::checkString($var, True);
        $this->stream_uri = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 default_pre_padding = 6;</code>
     * @return int
     */
    public function getDefaultPrePadding()
    {
        return $this->default_pre_padding;
    }

    /**
     * Generated from protobuf field <code>int32 default_pre_padding = 6;</code>
     * @param int $var
     * @return $this
     */
    public function setDefaultPrePadding($var)
    {
        GPBUtil::checkInt32($var);
        $this->default_pre_padding = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 default_post_padding = 7;</code>
     * @return int
     */
    public function getDefaultPostPadding()
    {
        return $this->default_post_padding;
    }

    /**
     * Generated from protobuf field <code>int32 default_post_padding = 7;</code>
     * @param int $var
     * @return $this
     */
    public function setDefaultPostPadding($var)
    {
        GPBUtil::checkInt32($var);
        $this->default_post_padding = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>bool recordable = 8;</code>
     * @return bool
     */
    public function getRecordable()
    {
        return $this->recordable;
    }

    /**
     * Generated from protobuf field <code>bool recordable = 8;</code>
     * @param bool $var
     * @return $this
     */
    public function setRecordable($var)
    {
        GPBUtil::checkBool($var);
        $this->recordable = $var;

        return $this;
    }

}

