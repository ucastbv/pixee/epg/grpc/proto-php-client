<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: epg/model/season.proto

namespace Tellie\Epg\Model;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>tellie.epg.model.Season</code>
 */
class Season extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string id = 1;</code>
     */
    protected $id = '';
    /**
     * Generated from protobuf field <code>optional int32 number = 2;</code>
     */
    protected $number = null;
    /**
     * Generated from protobuf field <code>repeated .tellie.epg.model.LocalizedField name = 3;</code>
     */
    private $name;
    /**
     * Generated from protobuf field <code>.tellie.epg.model.Show show = 4;</code>
     */
    protected $show = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $id
     *     @type int $number
     *     @type array<\Tellie\Epg\Model\LocalizedField>|\Google\Protobuf\Internal\RepeatedField $name
     *     @type \Tellie\Epg\Model\Show $show
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Epg\Model\Season::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>string id = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkString($var, True);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>optional int32 number = 2;</code>
     * @return int
     */
    public function getNumber()
    {
        return isset($this->number) ? $this->number : 0;
    }

    public function hasNumber()
    {
        return isset($this->number);
    }

    public function clearNumber()
    {
        unset($this->number);
    }

    /**
     * Generated from protobuf field <code>optional int32 number = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setNumber($var)
    {
        GPBUtil::checkInt32($var);
        $this->number = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated .tellie.epg.model.LocalizedField name = 3;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Generated from protobuf field <code>repeated .tellie.epg.model.LocalizedField name = 3;</code>
     * @param array<\Tellie\Epg\Model\LocalizedField>|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setName($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Tellie\Epg\Model\LocalizedField::class);
        $this->name = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.tellie.epg.model.Show show = 4;</code>
     * @return \Tellie\Epg\Model\Show|null
     */
    public function getShow()
    {
        return $this->show;
    }

    public function hasShow()
    {
        return isset($this->show);
    }

    public function clearShow()
    {
        unset($this->show);
    }

    /**
     * Generated from protobuf field <code>.tellie.epg.model.Show show = 4;</code>
     * @param \Tellie\Epg\Model\Show $var
     * @return $this
     */
    public function setShow($var)
    {
        GPBUtil::checkMessage($var, \Tellie\Epg\Model\Show::class);
        $this->show = $var;

        return $this;
    }

}

